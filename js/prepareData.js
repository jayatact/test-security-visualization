var getData = (function(){
	
	return {

		get_edge_list: function(nodes, master_pair_list){
			edge_list=[];
						master_pair_list.forEach(function(i){
							p1 = indexinhash(nodes,i["source"], "person");
							p2 = indexinhash(nodes,i["target"], "person");
							if(typeof p1 != "undefined" && typeof p2!= "undefined"){
								edge_list.push({"source": p1,"target": p2});
							}
						});
						//console.log(edge_list);
			return edge_list;

			function indexinhash(hasharray, value, key){

				var i=0;
				while(i<hasharray.length){
					if(hasharray[i][key]==value){
						return i;
					}
					i++;
				}
			}
		},

		get_node_list: function(criteria, value){
			var process_data = function(){
				personhash = {};

				var uniquearray = master_list.map(function (d) {
				       if (typeof personhash[d.person] == "undefined") {

				       		personhash[d.person] = 1;
				          return d;
				       }
				       else{
				      	return "Repeat";
				       }
				   });

				return uniquearray;
			};

			pair_list = process_data().filter(function(d){
					return d!="Repeat";
			});

			//Filter based on the given criteria, in case of Tableau viz it is the hscode
			if(criteria == 'undefined') {return pair_list;}
			else{
				
				temp_list = pair_list.filter(function(v){

					return v[criteria]==value;})

				return temp_list;
			}
		},


	
		search_value:function (list, property, value){
			//Search a list of objects for the property with value, return the object if found
			for(i=0;i<list.length;i++){
				if(list[i][property] == value){
					return list[i];
				}
			}
			
		}
	    
	}

	



}());