//AUTHOR: Jay Parchure
//ORGANIZATION: ACT
//DATE: September 2015
//Code to embed and interact with Tableau Dashboard in a web page 
function initializeViz(){ //Intialize Visualization
$('#filters').hide();
var placeholderDiv = document.getElementById("tableauViz");
var url = "http://tewtab01.corporate.act.org/#/site/individualworkspaces/views/Longitudinal_Sims_State_brett_0/EdgesNodesRatioTestCenterMap";
var options = {
   hideTabs: true, 
   width: "800px",
   height: "700px",
   onFirstInteractive: function() {
     // The viz is now ready and can be safely used.
      selectMarks(viz);
   }
};


var viz = new tableau.Viz(placeholderDiv, url, options); //Draw the viz on HTML

};
$(initializeViz); //jquery used to run initialization

//Implement marks selection
function selectMarks(viz){
  mainWorkbook = viz.getWorkbook();
  var worksheet = mainWorkbook.getActiveSheet();
    viz.addEventListener('marksselection', onMarksSelection);
    };



function onMarksSelection(marksEvent) {

  marksEvent.getMarksAsync().then(function(marks) {
            var links = marks[0].getPairs();
            if ( $('#networkViz').children().length > 0 ) { //If a network viz already exists
              $('#networkViz').empty(); //Remove it
            }
            $('#filters').show(); //Load the filters on marks selection
            $('#networkViz').show();
            drawViz("hsCodeAttending", links[2]["value"], "Composite");//Draw a new viz
          });
}
