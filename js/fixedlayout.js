var drawViz= function(filter, value, scoretype){

	var margin = {
    top : 10,
    right : 10,
    bottom : 10,
    left : 10
} 
	$("#networkViz").css("border", "1px solid rgb(199,199,199)");

  var w = 600-margin.right-margin.left,
      h = 600-margin.top-margin.bottom;
    //Load the canvas on which the viz would be drawn
	var svg = d3.select("#networkViz")
                  .append("svg")
                  .attr("width", w)
                  .attr("height", h)
                  .append('g').attr("id", "diagram");

     //Add reset button
     d3.select("#networkViz").append("button").text("Reset").attr("id", "reset").attr("value", "Reset");


     ///Only to fill blank area
     svg.append("rect")
		  .attr('fill','none')
		  .attr('pointer-events', 'all')
		  .attr("width", w + margin.left + margin.right)
		  .attr("height", h + margin.top + margin.bottom)
		  .attr("class","chartArea");


		svg.call(d3.behavior.zoom()
			.translate([0, 0])
			  .scale(1.0)
			  .scaleExtent([0.5, 8.0])
		  .on("zoom", function() {
		    d3.selectAll('#diagram').attr("transform", "translate(" + d3.event.translate[0] + "," + d3.event.translate[1] + ") scale(" + d3.event.scale + ")");

		  })
		  
		).on("dblclick.zoom", null);

//Radius of nodes
     var radius= 6;
    //Data Prep
   
	 master_pair_list= create_pair_list(scoretype);
	
	var nodes = getData.get_node_list(filter, value);

	var edges= getData.get_edge_list(nodes, master_pair_list);
	var ACTScoreType = getACTScoreField(scoretype);
	
		//Set color Scale
		var linearScale = d3.scale.quantize()
		.domain([4,36]).range(colorbrewer.Reds[8]);

		createLegend(linearScale);
		
		
		//Viz code	
		var pvalue = getpValueField(scoretype);
		var force = d3.layout.force()
			.nodes(nodes)
			.links(edges)
			.gravity(1)
            .distance(5)
            .charge(-100)
			.size([w,h])
			.start();

			//Draw links (edges)
		var link = svg.selectAll(".link")
			.data(edges)
			.enter()
			.append("line")
			.attr("class", "link")
			.attr("stroke-width", function(d){return (0.0001/d.source[pvalue])})
			//Add the fields you want to see about the PAIR on hover, in quotes INSIDE the square brackets below separated by space
			.on("mouseover", function(d){nodeTooltip(d,["Pair#", pvalue], "link")})
			.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; });
			
		var node_drag = d3.behavior.drag()
			.on("dragstart", dragstart)
			.on("drag", dragmove)
			.on("dragend", dragend);

			//Draw nodes
        var node = svg.selectAll("circle")
			.data(nodes)
			.enter()
			.append("circle")
			.attr("class", "node")
			.attr("r", 4.5)
			.attr("cx", function(d) { return d.x; })
			.attr("cy", function(d) { return d.y; })
			.attr("fill", function(d){;
						return linearScale(d[ACTScoreType]);
						})
			//Add the fields you want to see about the person on hover INSIDE the square brackets below separated by space
			.on("mouseover", function(d){nodeTooltip(d,[ACTScoreType, "Name"])}) 			
			.on("mouseout", function(d) {       
		            div.transition()        
		                .duration(500)      
		                .style("opacity", 0);   
		        })
			//.on("click", nodeClick)
			 .on('dblclick', connectedNodes)
			.call(node_drag);
 
			//DO NOT CHANGE THIS FUNCTION IF YOU WANT TO CHANGE THE DATA FIELD ON HOVER!
			//GO TO LINE 99 FOR PERSON INFO OR LINE 75 FOR PAIR INFO AND ADD FIELDS THERE
			//NOTE THAT THESE FIELDS SHOULD VERBATIM MATCH THE FIELDS IN THE 2015_Pairs csv file
		var nodeTooltip = function(d, params, toolType) {      
		            div.transition()        
		                .duration(200)      
		                .style("opacity", .9);      
		            div .html( function(){
		            	if(toolType === "link"){d=d["source"];}
		            	var htmlOutput = "";
		            	
		            	for(var i=0;i<params.length;i++){
		            		htmlOutput = htmlOutput + d[params[i]] + "<br/>" ;
		            	}
		            	return htmlOutput;

		            })  
		                .style("left", (d3.event.pageX) + "px")     
		                .style("top", (d3.event.pageY - 28) + "px")
		                .style("background", function(d){
		                	if(toolType== "link"){
		                		return "rgb(255,187,120)"; //Tooltip background for link hover
		                	}
		                	else{
		                		return "rgb(174,199,232)";//Tooltip background for node hover
		                	}
		                });    
		                              
		        };

/*
		 	function nodeClick(){
				if (!d3.select(this).classed("selected")) {
						col = d3.select(this).attr("fill");
					   d3.select(this)
					       .style('stroke', 'black')
					       .classed("selected",true)
					       .attr("fill", function(d){
					       		return getTintedColor(col,5);
					       })
					} 
					else {
						col = d3.select(this).attr("fill");
					  d3.select(this)
					      .style('stroke', 'white')
					      .classed("selected",false)
					      .attr("fill", function(d){
					       		return getTintedColor(col,-5);
					       })
					      
					}
			}*/


		    //CONNECTED NODE HIGHLIGHTING//

			//Toggle stores whether the highlighting is on
			var toggle = 0;
			var selectedNodeList=[];
			//Create an array logging what is connected to what
			var linkedByIndex = {};
			for (i = 0; i < nodes.length; i++) {
			    linkedByIndex[i + "," + i] = 1;
			};
			edges.forEach(function (d) {
			    linkedByIndex[d.source.index + "," + d.target.index] = 1;
			});

			//This function looks up whether a pair are neighbours  
			function neighboring(a, b) {
			    return linkedByIndex[a.index + "," + b.index];
			}

			function connectedNodes() {

			    if (toggle == 0) {
			        //Reduce the opacity of all but the neighbouring nodes
			        d = d3.select(this).node().__data__;
			        node.style("opacity", function (o) {
			        	if( neighboring(d,o)| neighboring(o,d)){
			        		if(selectedNodeList.indexOf(o)==-1){selectedNodeList.push(o);}
			        		if(selectedNodeList.indexOf(d)==-1){selectedNodeList.push(d);}

			        		
			        	}
			            return neighboring(d, o) | neighboring(o, d) ? 1 : 0.1;
			        });
			        link.style("opacity", function (o) {
			            return d.index==o.source.index | d.index==o.target.index ? 1 : 0.1;
			        });
			        showSelectedNodes(selectedNodeList);
			        toggle = 1;
			    } else {
			    	selectedNodeList=[];
			    	showSelectedNodes(selectedNodeList);
			        //Put them back to opacity=1
			        node.style("opacity", 1);
			        link.style("opacity", 1);
			        toggle = 0;
			    }

			}
			function showSelectedNodes(nodelist){
				$('#nodenames').remove();
				if(nodelist.length>0){

					var htmlOutput='<table><tr><th>Pair#</th><th>Name</th><th>PAS</th></tr>';
					for(var i=0;i<nodelist.length;i++){
						
						htmlOutput=htmlOutput + '<tr><td>' + nodelist[i]["Pair#"] 
									+ '</td><td>' + nodelist[i]["Name"]
									+ '</td><td>' + getData.search_value(master_pair_list, "Pair#", nodelist[i]["Pair#"])[pvalue]
									+ '</td></tr>'
					}
					htmlOutput = htmlOutput + '</table><br><button id="downloadpairs">Download</button>'
					d3.select('#filters').append('div').attr('id', 'nodenames').html(htmlOutput);
					var attrlist = ["Pair#", "Name", "PAS"];
					$('#downloadpairs').click(function(){downloadPairsCSV(nodelist, "pairlist.csv", attrlist)});
				}
			}
			function downloadPairsCSV(nodelist, filename, attrlist){
				//Source: http://halistechnology.com/2015/05/28/use-javascript-to-export-your-data-as-csv/
				    var data, filename, link;
			        var csv = convertArrayOfObjectsToCSV(nodelist, attrlist);
			        if (csv == null) return;

			        filename = filename || 'export.csv';

			        if (!csv.match(/^data:text\/csv/i)) {
			            csv = 'data:text/csv;charset=utf-8,' + csv;
			        }
			        data = encodeURI(csv);

			        link = document.createElement('a');
			        link.setAttribute('href', data);
			        link.setAttribute('download', filename);
			        link.click();
			}
			function convertArrayOfObjectsToCSV(data, keys) {  
			        var result, ctr, keys, columnDelimiter, lineDelimiter, data;

			        data =  data || null;
			        if (data == null || !data.length) {
			            return null;
			        }

			        columnDelimiter =  ',';
			        lineDelimiter = '\n';


			        result = '';
			        result += keys.join(columnDelimiter);
			        result += lineDelimiter;

			        data.forEach(function(item) {
			            ctr = 0;
			            keys.forEach(function(key) {
			                if (ctr > 0) result += columnDelimiter;

			                result += item[key];
			                ctr++;
			            });
			            result += lineDelimiter;
			        });

			        return result;
			}


			function getTintedColor(color, v) {
			    if (color.length >6) { color= color.substring(1,color.length)}
			    var rgb = parseInt(color, 16); 
			    var r = Math.abs(((rgb >> 16) & 0xFF)+v); if (r>255) r=r-(r-255);
			    var g = Math.abs(((rgb >> 8) & 0xFF)+v); if (g>255) g=g-(g-255);
			    var b = Math.abs((rgb & 0xFF)+v); if (b>255) b=b-(b-255);
			    r = Number(r < 0 || isNaN(r)) ? 0 : ((r > 255) ? 255 : r).toString(16); 
			    if (r.length == 1) r = '0' + r;
			    g = Number(g < 0 || isNaN(g)) ? 0 : ((g > 255) ? 255 : g).toString(16); 
			    if (g.length == 1) g = '0' + g;
			    b = Number(b < 0 || isNaN(b)) ? 0 : ((b > 255) ? 255 : b).toString(16); 
			    if (b.length == 1) b = '0' + b;
			    return "#" + r + g + b;
			}

  		//fisheyeView(); //Invoke this function if fisheye view is needed

  		//Code for fisheyeView implementation
  		//Warning: Do not use with zoom function
  		function fisheyeView(){
		var fisheye = d3.fisheye.circular()
		.radius(150);
		svg.on("mousemove", function() {
			if(zm<=1){
		      force.stop();
		      fisheye.focus(d3.mouse(this));
		      d3.selectAll("circle").each(function(d) { d.fisheye = fisheye(d); })
		          .attr("cx", function(d) { return d.fisheye.x; })
		          .attr("cy", function(d) { return d.fisheye.y; })
		          .attr("r", function(d) { return d.fisheye.z * 4.5; });
		      link.attr("x1", function(d) { return d.source.fisheye.x; })
		          .attr("y1", function(d) { return d.source.fisheye.y; })
		          .attr("x2", function(d) { return d.target.fisheye.x; })
		          .attr("y2", function(d) { return d.target.fisheye.y; });
		           }//close if statement
  			
		    })};


		function dragstart(d, i) {
			force.stop(); // stops the force auto positioning before you start dragging
		}

		function dragmove(d, i) {
			d.px += d3.event.dx;
			d.py += d3.event.dy;
			d.x += d3.event.dx;
			d.y += d3.event.dy; 
			tick(); 
		}

		function dragend(d, i) {
			d.fixed = true; //set the node to fixed so the force doesn't include the node in its auto positioning stuff
			tick();
		}

		force.on("tick", tick);

		//This snippet of code prevents initial floating of force layout
		var n=20;
      	for (var i = n * n; i > 0; --i) {force.tick();}
		force.stop();


		function tick() {//For the tick function of the Force Layout (See: https://github.com/mbostock/d3/wiki/Force-Layout) for more details
          link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

			node.attr("cx", function(d) { return d.x = Math.max(radius, Math.min(w - radius, d.x)); })
	        .attr("cy", function(d) { return d.y = Math.max(radius, Math.min(h - radius, d.y)); });
			}

		function getACTScoreField(scoreoption){

			var returnscoreType="";
			if(scoreoption =="Composite") {returnscoreType = "standardScaleScoreComposite";}
			if(scoreoption =="English") {returnscoreType = "standardScaleScoreEnglish";}
			if(scoreoption =="Math") {returnscoreType = "standardScaleScoreMath" ;}
			if(scoreoption =="Science") {returnscoreType = "standardScaleScoreScience";}
			if(scoreoption =="Reading") {returnscoreType = "standardScaleScoreReading";}

			return returnscoreType;
		}

		function getpValueField(scoreoption){

			var returnpValueType="";
			if(scoreoption =="Composite") {returnpValueType = "numberOfIdenticalResponseCompositePvalue";}
			if(scoreoption =="English") {returnpValueType = "numberOfIdenticalResponseEnglishPvalue";}
			if(scoreoption =="Math") {returnpValueType = "numberOfIdenticalResponseMathPvalue" ;}
			if(scoreoption =="Science") {returnpValueType = "numberOfIdenticalResponseSciencePvalue";}
			if(scoreoption =="Reading") {returnpValueType = "numberOfIdenticalResponseReadingPvalue";}

			return returnpValueType;
		}


		//Create color legend
		function createLegend(CScale){
			if($('.legendQuant').length == 0){
					d3.select("#filters")
		          .append("svg").append("g")
				  .attr("class", "legendQuant")

		  	var quantize = d3.scale.quantize()
		  			.domain([0,36])
		  			.range(d3.range(5).map(function(i) { return i ; }));

			var legend = d3.legend.color().labelFormat(d3.format("r")).scale(CScale);

			//Invoke legend
			d3.select(".legendQuant").call(legend);
			}
		 }


		var div = d3.select("body").append("div")   
    		.attr("class", "tooltip")               
    		.style("opacity", 0); //Append tooltip (hover window)

    	//Click function for the filter button
    	$('#scoreType > button').click(function(event){
		actScoreType = $('#scoreType > select').val();
		$('#networkViz').empty();
		$('#nodenames').empty();
		drawViz(filter, value, actScoreType);
		});

    	//Clear Viz
    	$('#reset').click(function(event){
    		$('#networkViz').hide();
    		$('#nodenames').hide();
    		$('#filters').hide();
    	})


};




	
	
